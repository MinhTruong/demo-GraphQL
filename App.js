/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react'
import {Platform, StyleSheet, Text, View, Button} from 'react-native'

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Hello</Text>
        <Button title={'Click me'} onPress={() => this.onPressButton()}/>
      </View>
    );
  }

  onPressButton = () => {
    this.fetchData('')
  }

  fetchData(data) {
    fetch('http://139.59.246.187:3000/graphql?', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }, 
      body: JSON.stringify({
        query: `mutation {
          signIn (
            email: "zodycashier@zodyapp.com"
              password: "1234567"
          ) {
            token
            refreshToken
            user {
              _id
              name
              avatar
              facebook
              gender
              business
            }
          }
        }`
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log('===>', responseJson)
    }).catch((error) => {
      console.log('Request api login error: ==>', error)
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  button: {
    width: 100,
    height: 30
  }
});
